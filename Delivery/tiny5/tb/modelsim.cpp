#include <cstdint>
#include <map>

extern "C" {
uint8_t mem_read8(uint32_t address);
void mem_write8(uint32_t address, uint8_t data);
uint16_t mem_read16(uint32_t address);
void mem_write16(uint32_t address, uint16_t data);
uint32_t mem_read32(uint32_t address);
void mem_write32(uint32_t address, uint32_t data);
}

static std::map<uint32_t, uint8_t> mem;

uint8_t mem_read8(uint32_t address)
{
	return mem[address];
}

void mem_write8(uint32_t address, uint8_t data)
{
	mem[address] = data;
}

uint16_t mem_read16(uint32_t address)
{
	return (mem[address + 1] << 8) | mem[address];
}

void mem_write16(uint32_t address, uint16_t data)
{
	mem[address] = data & 0xFF;
	mem[address + 1] = (data >> 8) & 0xFF;
}

uint32_t mem_read32(uint32_t address)
{
	return (mem[address + 3] << 24) | (mem[address + 2] << 16) |
		(mem[address + 1] << 8) | mem[address];
}

void mem_write32(uint32_t address, uint32_t data)
{
	mem[address] = data & 0xFF;
	mem[address + 1] = (data >> 8) & 0xFF;
	mem[address + 2] = (data >> 16) & 0xFF;
	mem[address + 3] = (data >> 24) & 0xFF;
}
