\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[toc,page]{appendix}
\usepackage{parskip}
\usepackage{float}
\usepackage{url}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{tikz}
\usepackage{tikz-timing}
\usepackage{minted}
\usepackage{csquotes}

\usetikzlibrary{calc,positioning,shapes,arrows,shadows}

\newcommand{\sectionbreak}{\clearpage}

\begin{document}

\pagenumbering{gobble}

\begin{titlepage}
	\centering
	\includegraphics[width=0.60\textwidth]{img/upc-positiu-p3005.png}
	\par
	\vspace{1cm}
	\large Universitat Politècnica de Catalunya - BarcelonaTech
	\par
	\large Facultat d'Informàtica de Barcelona
	\par
	\vspace{1cm}
	\large Master in Innovation and Research in Informatics
	\par
	\large High Performance Computing
	\par
	\large 2017 Q2
	\par
	\vspace{1cm}
	\LARGE Multiprocessors Architecture
	\par
	\vspace{1cm}
	\huge \textbf{RISC-V \& TileLink, Tiny5}
	\par
	\vspace{2cm}
	\large \textit{Authors:}
	\par
	\large Sergi Granell Escalfet
	\par
	\large Guillem López Paradís
	\par
	\vspace{1cm}
	\par
	\vfill
	\large \today \par
\end{titlepage}

\newpage
\tableofcontents
\newpage

\pagenumbering{arabic}

\section{Introduction}

In this document we present Tiny5: a system consisting of a RISC-V\cite{Riscv_spec} core with a memory connected through the SiFive's TileLink\cite{tl_spec} protocol. The basic idea has been to use the simplest element of the RISC-V ISA and the most basic subset of the TileLink protocol in order to implement the whole system from scratch.

This document is divided in three parts:
\begin{itemize}
    \item Section 2 introduces RISC-V
    \item Section 3 introduces TileLink
    \item Section 4 explains Tiny5
\end{itemize}

On one hand, the first two sections explain the theoretical basis of the technologies used in this project. First, an introduction of the RISC-V project is given and afterwards, TileLink is explained.

On the other hand, in the last section, some of the implementation details of the project and the decisions taken during its development are explained. Finally, there is some code and some diagrams of the system showing how all its components are working together.

\vspace{2cm}

\begin{figure}[H]
    \centering
    \includegraphics[width=7cm]{img/risc-v-logo.png}
    \caption{RISC-V logo}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=7cm]{img/sifive-logo.png}
    \caption{SiFive logo}
\end{figure}

\newpage

\section{RISC-V}
RISC-V is an open-source instruction set architecture (ISA) based on the reduced instruction set computing (RISC) architecture principles.

Even though the project began at the University of California, Berkeley, RISC-V has been designed from the beginning with fast, small, and low-power real-world implementations in mind.

A single RISC-V core might contain multiple hardware threads, called harts in the RISC-V context. Those harts implement an ISA and have a set of registers, which are explained in the following sections.

\subsection{ISA}
In order to support a wide range of applications, systems and architectures, the RISC-V ISA has a modular design which has parts of it defined as extensions. Each of those extensions has an assigned letter, being the Base Integer Instruction Set RV32/64/128I the base set of the RISC-V ISA. A reduced RV32I set with 16 bit registers called RV32E is specifically designed for small embedded systems in mind.
There are many other ISA extensions, which depending on the system needs, can be added into the implementation.

\subsubsection{Registers}
The RISC-V specification defines 31 integer registers, which depending on the architecture (RV32/64/128 or RV32E) will be 16, 32, 64 or 128 bits long. The register number zero is a read-only register with the constant 0.

The compiler can make use of this special register 0 to provide pseudo-instructions to make programming in assembly easier. One of such pseudo-instructions is \texttt{move rX, rY} which for instance can be translated as \texttt{add rX, rY, r0}.

Further extensions such as the floating point one, can add more registers available into the instruction set.

\subsubsection{Memory accesses}
Given the RISC-based design, memory access in RISC-V are done through special instructions which serve the load/store purposes. The memory view is \textit{little-endian} and addressed with 8-bit bytes.

RISC-V doesn't require memory accesses bigger than a byte to be naturally aligned, although in some implementations that might reduce the performance of the memory operations.

\subsubsection{Control transfer instructions}
RISC-V provides two types of control transfer instructions: unconditional jumps and conditional
branches.

Unconditional jumps are done through JAL and JALR instructions. JAL can target a $\pm 1$ MiB range from the current PC address. JAL also saved the PC+4 address into the destination register, which can be set to register 0 for a plain unconditional jump.

On the other hand, conditional branches provide a $\pm 4$ KiB range and always compare two registers before deciding if the branch should be taken.

\subsubsection{Atomic memory operations}
The base instruction set of RISC-V provides support for memory fences to enforce memory ordering (explained in the next section).

The atomic memory operation extension provides two groups of atomic instructions:
\begin{enumerate}
    \item Load-Reserved/Store-Conditional (LR/SC) pair.
    \item Atomic Memory Operations (AMO) which perform read-modify-write operations with multiprocessor synchronization.
\end{enumerate}

\subsection{Memory consistency model}
The memory consistency model defines a set of rules specifying the values that can be legally returned by memory loads. The RISC-V memory model specification defines the RISC-V Weak Memory Ordering (RVWMO) model and two extensions: \enquote{Zam} (misaligned atomics) and \enquote{Ztso} (Total Store Ordering).

A weak memory ordering model was chosen instead of a more strict one (e.g. sequential consistency) to provide enough flexibility for architects to build high-performance scalable designs while simultaneously supporting a tractable programming model.

Under RVWMO, code running on a hart appears to execute in order from the perspective of other memory instructions in the same hart, but memory instructions from another hart may observe the memory instructions from the first hart being executed in a different order, therefore, the RVWMO model defines a set of rules in terms of global memory order (ordering of the memory operations produced by all harts) that must be followed in order to comply with it:
\begin{itemize}
    \item Load Value Axiom: loads return the value written by the latest store to the same address.
    \item Atomicity Axiom: no store from another hart can appear in the global memory order between a paired LR and successful SC.
    \item Progress Axiom: no memory operation may be preceded in the global memory order by an infinite sequence of other memory operations.
\end{itemize}

\section{TileLink}
Tile Link is a chip-scale interconnect standard allowing multiple masters to connect with multiple slaves supporting coherent memory-mapped access to memory, providing low-latency and high throughput as well as a scalable neat design, which is suitable for multiprocessor systems and SoC's.

Like RISC-V, TileLink is free and open standard. It has been designed taking in mind the RISC-V architecture but can also support other ISAs. TileLink Provides the possibility of coherent access for masters with and without caches but it can also be scaled down to simple slave devices or scale up to high-throughput slaves.

Additionally, TileLink offers a cache-coherent shared memory by supporting a MOESI equivalent protocol. It also claims being deadlock free and has out of order completion as an improvement for concurrent operations. 

The next table shows the comparison and the ground that originated this new interconnect protocol. It was first designed in Berkeley, and then SiFive is the company behind this protocol.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
                      & AHB & Wishbone & AXI4 & ACE & CHI & TileLink \\ \hline
Open standard         & Y   & Y        & Y    & Y   & N   & Y        \\ \hline
Easy to implement     & N   & Y        &      & N   & ?   & Y        \\ \hline
Cache Block Motion    & N   & N        & N    & Y   & Y   & Y        \\ \hline
Multiple Cache layers & -   & -        & -    & N   & ?   & Y        \\ \hline
Reusable off-chip     & N   &          &      &     & ?   & Y        \\ \hline
High Performance      & N   & Y        & Y    &     & ?   & Y        \\ \hline
\end{tabular}
\caption{Comparison of interconnect standards}
\label{interconnect-std}
\end{table}

\subsection{Architecture}
The TileLink protocol architecture is defined as a graph of connected agents that are able to send messages over point-to-point channels within a link in order to perform operations on the shared memory address space. This protocol defines the following entities:

\begin{itemize}
    \item Agent: Any system that wants to communicate and hence participate in the protocol by sending or receiving messages: caches, CPU, accelerator, etc.
    \item Operation: The resulting modification permissions or data in memory (e.g. read and write).
    \item Messages: Set of control and data values to perform operations.
    \item Channel: A one-way communication connection between a master and slave that can  carry messages with the same priority\footnote{Inside a channel all the messages have the same priority but within channels some have more priority.}.
    \item Link: Set of channels between agents to perform operations.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=9cm]{img/tl_architecture.png}
    \caption{Basic TileLink Architecture}
    \label{basic-tl-arch}
\end{figure}

Figure \ref{basic-tl-arch} shows the concepts of agent, link, message, etc. This is the minimum TileLink communication between two agents but can grow up to more complex designs as shown in figure \ref{complex-tl-arch}. It is important to mention that this can introduce problems if the topology allows cycles, which will be explained later.

\begin{figure}[H]
    \centering
    \includegraphics[width=9cm]{img/tl_graph.png}
    \caption{Complex TileLink Architecture}
    \label{complex-tl-arch}
\end{figure}

An interesting point that gives us figure \ref{complex-tl-arch} is that a module can have multiple TileLink agents but without introducing cycles. An example of this could be a shared last level cache that is connected to different CPU's and memory banks.

\subsubsection{Network Topology}
TileLink supports any topology that can be described as a Directed Acyclic Graph (DAG), being links edges and vertices agents. This kind of topology is needed to prevent message loops that can deadlock the system. 

Figure \ref{tl-dag-not-allowed-link} shows a clear example of a not allowed link (red link) between agents because if such link existed, then there would be a cycle, hence not satisfying the requirement of DAG and being open to deadlock problems. 

\begin{figure}[H]
    \centering
    \includegraphics[width=9cm]{img/tl_not_allowed.png}
    \caption{TileLink DAG example, with a forbidden red link}
    \label{tl-dag-not-allowed-link}
\end{figure}

\subsubsection{Protocol subsets}

Another important point of the TileLink architecture is the subsets of the protocol it offers. TileLink is divided in three different subsets, allowing to have different types of agents with more or less complexity:

\begin{enumerate}
    \item TileLink Uncached Lightweight (TL-UL)
    \item TileLink Uncached Heavyweight (TL-UH)
    \item TileLink Cached (TL-C)
\end{enumerate}

TL-UL is the simplest one and the next two adds operations on top of the TL-UL. It is designed in this way to allow having simpler agents that may not require cache coherence.

The next table shows the messages that each subset of the protocol offers. TL-UL is the simplest offering only common memory read and write operations (Get/Put).

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|}
\hline
Operations           & TL-UL & TL-UH     & TL-C \\ \hline
Get/Put accesses     & Y     & Y         & Y    \\ \hline
Hint Operations      & -     & Y         & Y    \\ \hline
Atomic accesses      & -     & Y         & Y    \\ \hline
Multibeat Operations & -     & Y         & Y    \\ \hline
Channels B+C+E       & -     & -         & Y    \\ \hline
Cache line transfers & -     & -         & Y    \\ \hline
\end{tabular}
\caption{Subsets of TileLink}
\label{tilelink-ops-table}
\end{table}

It is important to mention that these three subsets of the protocol can coexist in the same system. Also, these operations will have messages associated with it, shown in the next table. We can also see how most of the complexity of TileLink is introduced by TL-C.

\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{img/tl_messages.png}
    \caption{TileLink messages}
    \label{tl-messages}
\end{figure}

Another point that table \ref{tl-messages} shows is that there are only 5 channels within a Link between two agents. These 5 channels are only used if the TL-C is used, so for the case of TL-UL, only two channels (A and D) will be used and hence reducing the resources used by this simplest subset of the protocol.

\section{Tiny5}

Tiny5 is a RISC-V based system with a single-core in-order RISC-V CPU implementing almost\footnote{Some instructions such as \texttt{ERET} and \texttt{ECALL} are misisng.} all of the RV32I instruction set and a memory connected through TileLink UL to the CPU.

The implementation is done by using the hardware description language SystemVerilog, and the simulation can be done either by using the free and open-source Verilator\footnote{\url{https://www.veripool.org/wiki/verilator}} tool or Mentor Graphics's ModelSim\footnote{\url{https://www.mentor.com/products/fpga/verification-simulation/modelsim/}}.

In summary, Tiny5 has consisted in taking the simplest RISC-V extension set (RV32I) and the simplest TileLink subset of the protocol (TL-UL). This has been our proof of concept project and we believe RISC-V and TileLink have an interesting future thanks in part to their openness.

In the following sections we will explain the steps and implementation decisions we have taken during the implementation of this project.

\subsection{RISC-V implementation}

Our implementation is based on the Von Neumann architecture, where the same memory stores both the code to be executed and the data. Therefore, in order to avoid structural hazards when accessing the memory, we have decided to pipeline the CPU in two main stages \texttt{FETCH} and \texttt{DEMW}. Those two pipeline stages correspond to CPU states in addition to the \texttt{RESET} state, as explained as follows:

\begin{itemize}
    \item \texttt{RESET}: Initial state when the reset signal is asserted. Once the reset signal is deserted it proceeds to the \texttt{FETCH} state.
    \item \texttt{FETCH}: This state issues a memory load to the address pointed by the PC register in order to retrieve the instruction to be executed.
    \item \texttt{DEMW}: \enquote{Decode-Execution-Memory-Writeback}, it corresponds to the execution of the instruction loaded in the \texttt{FETCH} stage.
\end{itemize}

To make the implementation easier, we have opted for a non-overlapping pipeline design, where only one stage is being executed at any point of time, and no instruction starts executing until the previous one has finished, as shown in figure \ref{tiny5-cpu-simple-pipeline}.
\begin{figure}[H]
    \centering
    \begin{tikztimingtable}
        clock & [C] 12{3C} \\
        Instr 0 & G 6D{FETCH} 6D{DEMW} G \\
        Instr 1 & 6S 6S G 6D{FETCH} 6D{DEMW} G \\
        Instr 2 & 6S 6S 6S 6S G 6D{FETCH} 6D{DEMW} G \\
        ... \\
        \extracode
            \begin{pgfonlayer}{background}
            \begin{scope}[semitransparent,semithick]
                \vertlines[red]{0, 12, ..., 36}
                \vertlines[blue]{6, 18, ..., 30}
            \end{scope}
            \end{pgfonlayer}
    \end{tikztimingtable}
    \caption{Tiny5 CPU pipeline}
    \label{tiny5-cpu-simple-pipeline}
\end{figure}

The CPU logic follows a split into two parts: a pure combinational part plus the state register (control logic) and the datapath, which ties together all the components (register file, control logic, ALU, compare unit, and the CSR register file) and also holds the PC register and the instruction register.
Consequently, the control logic only needs the instruction register as an input, and according to it and the current state it outputs all the signals that control the multiplexors in the datapath (e.g. the one that selects if the input to the register file should be the output of the ALU or the data read from memory).

\subsubsection{Memory interface \textit{tinymemif}}

Initially, and for the sake of easiness, the interface that connected the CPU and memory was a custom very simple one that we call \textit{tinymemif}, which only consists of the following signals: \texttt{rd\_addr}, \texttt{rd\_data}, \texttt{rd\_size}, \texttt{rd\_enable} for the read part, \texttt{wr\_addr}, \texttt{wr\_data}, \texttt{wr\_size}, \texttt{wr\_enable} for the write part, and \texttt{busy} which tells when memory is busy performing an operation.

\subsection{TileLink UL implementation}

Changing the interface to memory and implementing TileLink ended up being a more difficult task than initially thought. It not only required changing the memory interface part of the CPU (\textit{tinymemif}) but also touching the core of it in order to add more stages to the pipeline to be able to make the CPU halt until the TileLink messages complete: \texttt{FETCH\_ISSUE}, \texttt{FETCH\_WAIT}, \texttt{DEMW\_ISSUE}, \texttt{DEMW\_WAIT}.

To avoid adding all the TileLink complexity inside the CPU, we have decided to encapsulate TileLink in its own module where the complex finite state machine that controls it is implemented. This module acts a \textit{tinymemif} $\leftrightarrow$ TileLink bridge on both the CPU and the memory array agents.
In figure \ref{tl_connection_diagram} we show a block diagram of such connections. In the diagram, the black arrow shows the links using our \textit{tinymemif} interface, and the blue arrow is the connection between the TileLink UL agents. On the arrows, the \enquote{M} stands for the master side of the link and \enquote{S} the slave side of the connection.

\begin{figure}[H]
	\centering
	\input{figure/tl_diagram.tex}
	\caption{Tiny5 TileLink memory connection diagram}
	\label{tl_connection_diagram}
\end{figure}

In the following subsections we will present waveform diagrams which show the execution of the TileLink \texttt{Get} and \texttt{Put} operations. A disassembly of the source code used to generate those diagrams is shown in figure \ref{asm_code}, and in particular we will use the \texttt{FETCH} stage of the \texttt{sw} instruction at line 11 to show the Get operation and the \texttt{DEMW} stage of the same instruction for the Put operation.

\begin{figure}[H]
    \inputminted[frame=single,fontsize=\scriptsize,linenos,obeytabs=true,tabsize=8,bgcolor=lightgray,highlightlines={11}]{text}{asm/disasm.txt}
    \caption{Sample assembly code}
    \label{asm_code}
\end{figure}

\subsubsection{TileLink \texttt{Get} waveform}

The CPU is fetching the \texttt{sw} instruction from the address \texttt{0x1001c}, therefore the TileLink operation will be a \texttt{Get} (\texttt{opcode 4}), which according to the TileLink specification goes through Channel A and has the following important fields in this case:
\begin{description}
    \item[a\_opcode] \texttt{4}, \texttt{Get} operation.
    \item[a\_param] \texttt{0}, reserved
    \item[a\_size] \texttt{2}, $2^2 = 4$ bytes
    \item[a\_address] \texttt{0x1001c}, the target address.
    \item[a\_mask] \texttt{0b1111}, use all the 4 byte lanes.
\end{description}

The response to the Get operation goes through Channel D and is an \texttt{AccessAckData} (\texttt{opcode 1}) carrying the retrieved data on the \texttt{d\_data} field of the channel.

\begin{figure}[H]
    \centering
    \centerline{\includegraphics[width=16cm]{img/waveform_get.png}}
    \caption{Tiny5 TileLink \texttt{Get} message}
    \label{tiny5-tl-get}
\end{figure}

\subsubsection{TileLink \texttt{Put} waveform}

Once the CPU has fetched the instruction, now the \texttt{sw} executes which is a store operation of the register \texttt{x4} (containing \texttt{0x0000aa00}) to \texttt{0(x5)} (effective address \texttt{0x10024}). In this case the TileLink operation is Put with message \texttt{PutFullData} (\texttt{opcode 0}) that also goes through Channel A. The relevant fields of this message are the following:
\begin{description}
    \item[a\_opcode] \texttt{0}, \texttt{PutFullData} operation.
    \item[a\_param] \texttt{0}, reserved
    \item[a\_size] \texttt{2}, $2^2 = 4$ bytes
    \item[a\_address] \texttt{0x10024}, the target address.
    \item[a\_mask] \texttt{0b1111}, use all the 4 byte lanes.
    \item[a\_data] \texttt{0x0000aa00}, data payload to be written.
\end{description}

For this operation the response is \texttt{AccessAck} (\texttt{opcode 0}) and also goes through Channel D.

\begin{figure}[H]
    \centering
    \centerline{\includegraphics[width=16cm]{img/waveform_put.png}}
    \caption{Tiny5 TileLink \texttt{Put} message}
    \label{tiny5-tl-put}
\end{figure}

\newpage

\begin{appendices}

\section{Tiny5 instructions}

The source code is structured in three main parts:
\begin{enumerate}
    \item Tiny5 implementation: all the \texttt{.sv} files in the top directory.
    \item Testbench code: inside the \texttt{tb/} folder, it has the required C++ Verilator hooks that link Tiny5 with it.
    \item CPU assembly code to execute: inside the \texttt{test/} folder.
\end{enumerate}

To compile the assembly test code, GCC\footnote{\url{https://gcc.gnu.org/}} is used, and in particular the \\ \texttt{riscv64-unknown-elf} flavor of the GNU toolchain is required.

A Makefile is provided in order to perform all the necessary operations:
\begin{itemize}
    \item \texttt{make lint}: Checks the code syntax by using Verilator.
    \item \texttt{make verilate}: Compiles the Tiny5 code into a runnable executable by using Verilator.
    \item \texttt{make run}: Compiles Tiny5 and runs it by loading the test code.
    \item \texttt{make gtkwave}: Runs the code and opens the trace with gtkwave\footnote{\url{http://gtkwave.sourceforge.net/}}.
    \item \texttt{make modelsim}: Opens ModelSim for simulation.
    \item \texttt{make disasm\_test}: Shows a disassembly (by using GCC's \texttt{objdump}) of the test program.
\end{itemize}

The C++ program we have programmed to interface with Verilator is designed in a way that accepts multiple command line parameters:
\begin{itemize}
    \item \texttt{-l, --load addr=ADDR,file=FILE} loads \texttt{FILE} to \texttt{ADDR}.
    \item \texttt{-m, --max-ticks=N} maximum number of ticks before stopping.
\end{itemize}

\end{appendices}

\bibliographystyle{plain}
\bibliography{references}

\end{document}
